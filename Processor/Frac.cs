﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processor
{/*
    public class Frac
    {
        int num;
        int denom;
        public int Num
        {
            get { return num; }
            set { if (num != value) num = value; }
        }
        public Frac(int num, int denom)
        {
            this.num = num;
            this.denom = denom;
        }
        public Frac()
        {
            num = 0;
            denom = 1;
        }
        public override string ToString() //Исключение
        {
            //int nod = Nod(this.num, this.denom);
            //Console.WriteLine("nod=", nod);
            //num /= nod;
            //denom /= nod;
            if (denom == 0) throw new Exception("0 в знаменателе");
            return num + "/" + denom;
        }
        public Frac Add(Frac b)
        {
            int n = this.num * b.denom + b.num * this.denom;
            int d = this.denom * b.denom;
            return new Frac(n, d);
        } //Сложение
        public static Frac operator +(Frac a, Frac b)
        {
            int n = a.num * b.denom + b.num * a.denom;
            int d = a.denom * b.denom;
            return new Frac(n, d);
        }
        public Frac Sub(Frac b)
        {
            int n = this.num * b.denom - b.num * this.denom;
            int d = this.denom * b.denom;
            return new Frac(n, d);
        } //Вычитание
        public Frac Mul(Frac b)
        {
            int n = this.num * b.num;
            int d = this.denom * b.denom;
            return new Frac(n, d);
        } //Умножение
        public Frac Dvd(Frac b)
        {
            int n = this.num * b.denom;
            int d = this.denom * b.num;
            return new Frac(n, d);
        } //Деление
        static int nod(int a, int b)
        {
            int t;
            a = Math.Abs(a);
            b = Math.Abs(b);
            if (a > b)
            {
                t = a;
                a = b;
                b = t;
            }
            while (a != 0 & a != b)
            {
                if (a < b)
                    b = b % a;
                else
                {
                    t = a;
                    a = b;
                    b = t;
                }
            }
            return b;
        }
        void Reduce()
        {
            int t = nod(this.num, this.denom);
            this.num /= t;
            this.denom /= t;
        } //Сокращение
    }*/
}
