﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Editor
    {
        string number = ""; //Поле для хранения редактируемого числа.
        const string sign = "-"; //Знак.
        const char delim = '/'; //Разделитель целой и дробной частей.
        const string zero = "0"; //Ноль.
        public string Number //Свойство для чтения редактируемого числа. 
        {
            get
            {
                if (number == "")
                    return "0/1";
                return number;
            }
        }
        public string AddDigit(int n) //Добавить цифру.
        {
            return number += n;
        }
        public string AddZero() //Добавить ноль.
        {
            if (number == "")
                return Number;
            else
                if (number.Last() == '/')
                return Number;
            number += 0;
            return Number;
        }
        public string AddDelim() //Добавить разделитель.
        {
            if (number == "" | number.Contains('/'))
                return Number;
            else number += '/';
            return Number;
        }
        public string AddSign() //Добавить знак.
        {
            if (number.Contains("-"))
            {
                return number = number.Remove(0, 1);
            }
            else
            {
                if (number == "")
                {
                    return number;
                }
                else
                    return number = number.Insert(0, "-");
            }
        }
        public string Bs() //Удалить символ cправа.
        {
            if (number.Length != 0)
            {
                if (number.Length == 2 & number.Contains("-"))
                    return number = Number.Remove(0, 2);
                else
                {
                    number = number.Remove(number.Length - 1, 1);
                    return Number;
                }
            }
            else
                return Number;
        }
        public string Clear() //Очистить редактируемое число.
        {
            if (number == null)
                return "0/1";
            else
            {
                number = "";
                return Number;
            }
        }
        public string DoEdit(int j) //Выполнить команду редактирования.
        {
            if (j > 0 && j < 10)
                return AddDigit(j);
            else
                switch (j)
                {
                    case 0:
                        return AddZero();
                    case 10:
                        return AddDelim();
                    case 11:
                        return AddSign();
                    case 12:
                        return Bs();
                    case 13:
                        return Clear();
                    default:
                        return Number;
                }
        }
    }
}
