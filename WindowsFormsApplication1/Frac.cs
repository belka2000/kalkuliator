﻿using System;
using System.Windows.Forms;
namespace Processor
{
    public class Frac
    {
        int num;
        int denom;
        public Frac(string n) //Split разбивает строку на их массив
        {
            if (n.Contains("/"))
            {
                string[] s = n.Split('/');
                num = int.Parse(s[0]);
                denom = int.Parse(s[1]);
            }
            else
                return;
        }
        public Frac(int num, int denom)
        {
            this.num = num;
            this.denom = denom;
        }
        public Frac()
        {
            num = 0;
            denom = 1;
        }
        public override string ToString() //Исключение
        {
            if (denom == 0)
            {
                MessageBox.Show("0 в знаменателе", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                denom = 1;
                // Либо можно исключение
                // throw new Exception("0 в знаменателе");
            }
            Reduce();
            return num + "/" + denom;
        }
        public Frac Add(Frac b)
        {
            int n = num * b.denom + b.num * denom;
            int d = denom * b.denom;
            return new Frac(n, d);
        } //Сложение
        public static Frac operator +(Frac a, Frac b)
        {
            int n = a.num * b.denom + b.num * a.denom;
            int d = a.denom * b.denom;
            return new Frac(n, d);
        }
        public Frac Sub(Frac b)
        {
            int n = num * b.denom - b.num * denom;
            int d = denom * b.denom;
            return new Frac(n, d);
        } //Вычитание
        public Frac Mul(Frac b)
        {
            int n = num * b.num;
            int d = denom * b.denom;
            return new Frac(n, d);
        } //Умножение
        public Frac Dvd(Frac b)
        {
            int n = num * b.denom;
            int d = denom * b.num;
            if (d < 0)
            {
                    d *= -1;
                    n *= -1;
            }
            
            return new Frac(n, d);
        } //Деление
        static int Nod(int a, int b)
        {
            int t;
            a = Math.Abs(a);
            b = Math.Abs(b);
            if (a > b)
            {
                t = a;
                a = b;
                b = t;
            }
            while (a != 0 & a != b)
            {
                if (a < b)
                    b = b % a;
                else
                {
                    t = a;
                    a = b;
                    b = t;
                }
            }
            return b;
        }
        void Reduce()
        {
            int t = Nod(num, denom);
            num /= t;
            denom /= t;
        } //Сокращение
    }
}
