﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        enum State { Редактирование, Вычислено }
        State state = State.Редактирование;
        Processor.Proc P = new Processor.Proc();
        Editor E = new Editor();
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e) //Обработчик событий
        {
            LeftOprnd.Text = P.LeftOprnd.ToString();
            RightOprnd.Text = P.RightOprnd .ToString();
            InpResult.Text = P.Result.ToString();
        }
        private void Common_Click(object sender, EventArgs e) //Все команды редактирования
        {
            Button b = sender as Button;
            string s = b.Tag.ToString();
            int com = int.Parse(s);
            //InpResult.Text = E.DoEdit(com);
            Do(com);
        }
        private void Do(int com)
        {
            if (com >= 0 & com <= 12)//Команда редактирования.
            {
                if (state == State.Вычислено)
                {
                    LeftOprnd.Text = E.Clear();
                    RightOprnd.Text = E.Clear();
                    InpResult.Text = E.Clear(); //Очистить редактор и поле ввода.
                    state = State.Редактирование; //Установить режим редактирования.
                }
                InpResult.Text = E.DoEdit(com); //Выполнить команду редактирования и отобразить результат.
                return;
            }
            if (com == 13)
            {
                P.LeftOprnd = new Processor.Frac();
                P.RightOprnd = new Processor.Frac();
                P.Result = new Processor.Frac();
                P.Op = null;
                Operation.Text = null;
                LeftOprnd.Text = E.Clear();
                RightOprnd.Text = E.Clear();
                InpResult.Text = E.Clear();
            } //Очистить
            if (com >= 14 & com <= 17) //Выбрана операция.
            {
                P.Op = com.ToString(); //Записать выбранную операцию.
                Operation.Text = P.Op; //Отобразить выбранную операцию.
                P.LeftOprnd = new Processor.Frac(E.Number); //Записать левый операнд в процессор.
                LeftOprnd.Text = P.LeftOprnd.ToString(); //Отобразить левый операнд.
                InpResult.Text = E.Clear(); //Очистить редактор и поле ввода
                return;
            }
            if (com == 19) //Выполнить операцию.
            {
                if (state == State.Вычислено)
                {
                    P.LeftOprnd = new Processor.Frac(InpResult.Text); //Записать результат в левый операнд.
                    LeftOprnd.Text = P.LeftOprnd.ToString(); //Отобразить левый операнд.
                }
                else
                {
                    P.RightOprnd = new Processor.Frac(InpResult.Text); //Записать правый операнд в процессор.
                    RightOprnd.Text = P.RightOprnd.ToString(); //Отобразить правый операнд.
                    if (P.RightOprnd.ToString() == "0/1")
                    {
                            MessageBox.Show("Вы ввели не все данные", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            return;
                    }
                }
                InpResult.Text = P.DoOprtn().ToString(); //Выполнить операцию и отобразить результат.
                state = State.Вычислено; //Установить состояние: операция выполнена.
                return;
            }
        }
        /*enum State { Редактирование, Вычислено }

//Состояние калькулятора.
State state = State.Редактирование;
//Редактор.
Editor E = new Editor();
//Процессор.
Processor.Proc P = new Processor.Proc();
public Form1()
{
InitializeComponent();
}

//Инициализация компонентов формы при загрузке. 
private void Form1_Load(object sender, EventArgs e)
{
P.LeftOprnd = P.LeftOprnd;
RightOpnd.Text = P.B.ToString();
InpResult.Text = P.C.ToString();
}
//Все команды.
private void common_Click(object sender, EventArgs e)
{
Button b = (Button)sender;
int cmd = int.Parse(b.Tag.ToString());
Do(cmd);
}
//Выполнение всех команд.
private void Do(int cmd)
{
if (cmd >= 0 & cmd <= 13)//Команда редактирования.
{
if (state == State.Вычислено)
{
  //Очистить редактор и поле ввода.
  InpResult.Text = E.Clear();
  //Установить режим редактирования.
  state = State.Редактирование;
}
//Выполнить команду редактирования и отобразить результат.
InpResult.Text = E.DoEdit(cmd);
return;
}
if (cmd >= 14 & cmd <= 17) //Выбрана операция.
{
//Записать выбранную операцию.
P.op = cmd.ToString();
//Отобразить выбранную операцию.
Oprtn.Text = P.op;
//Записать левый операнд в процессор.
P.A = new Processor  Frac(E.Number);
//Отобразить левый операнд.
LeftOpnd.Text = P.A.ToString();
//Очистить редактор и поле ввода
InpResult.Text = E.Clear();
}
if (cmd == 18) //Выполнить операцию.
{
//Записать правый операнд в процессор.
P.B = new Frac(InpResult.Text);
//Отобразить правый операнд.
RightOpnd.Text = P.B.ToString();
//Выполнить операцию и отобразить результат.
InpResult.Text = P.DoOprtn().ToString();
//Установить состояние: операция выполнена.
state = State.Вычислено;
}

}

//Выполнение команд с клавиатуры.
private void Form1_KeyDown(object sender, KeyEventArgs e)
{
int cmnd = 0;
//Команды, соответствующие вводу цифр с АЦК.
if (e.KeyCode >= Keys.D0 & e.KeyCode <= Keys.D9) cmnd = (int)(e.KeyCode - Keys.D0);
//Команды, соответствующие вводу цифр с ЦК.
if (e.KeyCode >= Keys.NumPad0 & e.KeyCode <= Keys.NumPad9) cmnd = (int)(e.KeyCode - Keys.NumPad0);
//Команды, соответствующие вводу команд редактирования и выполнения операций.
switch (e.KeyCode)
{
case Keys.OemBackslash: cmnd = 10; break;
case Keys.OemMinus: cmnd = 11; break;
case Keys.Back: cmnd = 12; break;
case Keys.Delete: cmnd = 13; break;
case Keys.Add: cmnd = 14; break;
case Keys.Subtract: cmnd = 15; break;
case Keys.Multiply: cmnd = 16; break;
case Keys.Divide: cmnd = 17; break;
case Keys.Enter: cmnd = 18; break;
  //default: cmnd = 0; break;
}
Do(cmnd);
}
//Меню Выход.
private void toolStripMenuItem1_Click(object sender, EventArgs e)
{
Close();
}

*/
    }
}